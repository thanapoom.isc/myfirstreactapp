import React, { Component } from 'react'
import './App.css';
import Person from './Person/Person';
import UserInput from './UserInput/UserInput';
import UserOutput from './UserOutput/UserOutput';

export class App extends Component {
  state = {
    persons: [
      { name: 'a', age: 1 },
      { name: 'b', age: 2 },
      { name: 'c', age: 3 }
    ],
    users: [
      { username: "1" },
      { username: "2" },
      { username: "3" },
      { username: "4" }
    ],
    otherState: 'wow!'
  }

  changeUsername = (event) => {
    this.setState({
      users:[
        { username: event.target.value },
        { username: "2" },
        { username: "3" },
        { username: "4" }
      ]
    })
  }

  swithNameHandler = (newName) => {
    this.setState({
      persons: [
        { name: newName, age: 1 },
        { name: 'b', age: 5 },
        { name: 'c', age: 6 }
      ]
    })
  }

  nameChangedHandler = (event) => {
    this.setState({
      persons: [
        { name: 'mr 1', age: 12 },
        { name: event.target.value, age: 12 },
        { name: 'xxx', age: 12 },
      ]
    })
  }

  render() {
    const style = {
      backgroundColor: 'blue',
      font: 'inherit',
      border: '1px solid black',
      padding: '8px',
      cursor: 'pointer'
    }

    return (
      <div className="App">
        <h1>Hi I'm a React app</h1><br />
        <button
          style={style}
          onClick={this.swithNameHandler.bind(this, 'Wow!')}> Switch things
        </button>
        <Person
          name={this.state.persons[0].name}
          age={this.state.persons[0].age}> </Person>
        <Person
          name={this.state.persons[1].name}
          age={this.state.persons[1].age}
          clicked={this.swithNameHandler.bind(this, 'Wow2!!')}
          changed={this.nameChangedHandler}> Hobby: Diving </Person>
        <Person
          name={this.state.persons[2].name}
          age={this.state.persons[2].age}> </Person>
        {this.state.otherState}
        <UserInput changed={this.changeUsername.bind(this)}></UserInput>
        <UserOutput username={this.state.users[0].username}></UserOutput>
        <UserOutput username={this.state.users[1].username}></UserOutput>
        <UserOutput username={this.state.users[2].username}></UserOutput>
        <UserOutput username={this.state.users[3].username}></UserOutput>
      </div>
    )
  }
}

export default App
