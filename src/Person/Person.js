import React, { Component } from 'react'
import './Person.css';

export class Person extends Component {
    render() {
        return (
            <div className="Person">
                <p onClick={this.props.clicked}>
                    I'm a person {this.props.name} age: {this.props.age}
                    {this.props.children}
                </p>
                <input type="text"
                    onChange={this.props.changed}
                    value={this.props.name}>
                </input>
            </div>
        )
    }
}

export default Person

